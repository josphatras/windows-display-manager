import { Component, OnInit, ViewChild,HostListener, ElementRef, AfterViewInit, Renderer2, Inject } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { PresentationService } from '../presentation.service';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field'; 
import {MatSelectModule} from '@angular/material/select';
import {MatListModule} from '@angular/material/list';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SysConfig } from '../../config/sys';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDialogModule, MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {lanTranslation} from '../../config/lan';
import  { ActivatedRoute,  Router, Routes, RouterModule } from '@angular/router';
import { NotificationsService } from '../../notifications/notifications.service';

export interface DialogData {
  id: string;
  name: string;
}


@Component({
  selector: 'app-add-template',
  templateUrl: './add-template.component.html',
  styleUrls: ['./add-template.component.css']
})
export class AddTemplateComponent implements OnInit {
  screenWidth = 600;
  displayRatio = 1;
  screenResolution = {"x":0,"y":0};
  selcetedScreenId: any;
  wins = [];
  addNew = false;
  drawInProgress = false;
  selected = false;
  draw_controles = false;
  selectedWin: any;
  screens : any;
  startX: any;
	startY: any;
	templates: any;
	serverLink: any;
	screenId: any;
	modeselect: any;
	serverData: any;
	templateId: any;
	action: any;
	new_btn = true;
	update_btn = false;
	winEditForm = new FormGroup({
		screen: new FormControl(''),
		name: new FormControl(''),
		color: new FormControl(''),
		pixelX: new FormControl(''),
		pixelY: new FormControl(''),
		pixelWidth: new FormControl(''),
		pixelHeight: new FormControl('')
	});
	lang = lanTranslation;
  /** Canvas 2d context */
  private context: CanvasRenderingContext2D;
  private htmlEl: any;
	@ViewChild('colorSelection') colorSelection:ElementRef;
  constructor(
		private renderer: Renderer2,
		private el: ElementRef,
		private fb: FormBuilder,
		private route: ActivatedRoute,
		private rt: Router,		
		private http: HttpClient,
		private data: PresentationService,
		public dialog: MatDialog,
		private notificationsService: NotificationsService,
	) { }
	form: FormGroup;	
	@ViewChild('canvasEl') canvasEl: ElementRef;
	@ViewChild('tempManage') tempManage: ElementRef;
	@ViewChild('addTemp') addTemp: ElementRef;

  ngOnInit(): void {
		this.serverLink = SysConfig.siteUrl;
		this.data.screens().subscribe(
					(val) => {
						this.screens = val;
						if(this.screens["0"] != undefined){
							this.getTemplates(this.screens["0"].id);
							this.modeselect = 26;
						}
			},
			response => {

			},
			() => {
				// console.log("The observable is now completed.");
			  }
			);   
		this.route.params.subscribe(params=>{
			this.templateId = params.templateId;
			this.action = params.action;
			if (this.action == 'edit') {
				this.new_btn = false;
				this.update_btn = true;
			}else{
				this.new_btn = true;
				this.update_btn = false;
			}
		});

		this.notificationsService
		  .templatesObserver()
		  .subscribe((message: string) => {
		  	console.log(this.screenId);
				this.getTemplates(this.screenId);
				console.log(message);
		});			
  }

	setColor(){
		var colorEl = this.colorSelection.nativeElement as HTMLCanvasElement;
		colorEl.style.background = "rgba(" + this.winEditForm.value.color + ", 1)";
		this.wins[this.selectedWin].color = this.winEditForm.value.color;
		this.drawWindows();
	}


  ngAfterViewInit() {
		this.htmlEl = this.canvasEl.nativeElement as HTMLCanvasElement;
/*			this.renderer.setStyle(
	   this.el.nativeElement, 'width', "50"
	   );*/
		this.context = this.htmlEl.getContext('2d');
		this.drawWindows();	
		//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	}

  drop(event: CdkDragDrop<string[]>) {
		moveItemInArray(this.wins, event.previousIndex, event.currentIndex);
		this.selectedWindow(event.currentIndex)
		this.drawWindows();
  }

  editTemplateSec(templateId){
  	this.rt.navigate(['/channel-templates/edit/' + templateId]);
  	this.initiateEdit();
  }

  initiateEdit(){
  	this.addTemp.nativeElement.style.display= "block";
		this.tempManage.nativeElement.style.display= "none";
		this.draw_controles = true;
		this.data.screens().subscribe(
					(val) => {
						this.screens = val;
						this.data.templateEditData(this.templateId).subscribe(
							(val) => {
									this.serverData = val;
									var screenSelection = "";
									this.wins = [];
									for (var i = 0; i < this.screens.length; i++) {
										if (this.screens[i].id == this.serverData.screen_id) {
											screenSelection = this.screens[i].id + "X" + this.screens[i].resolution.x + "X" + this.screens[i].resolution.y;
											this.screenSelection(screenSelection);
											this.winEditForm.patchValue({
												screen: screenSelection,
												name: this.serverData.name,
											});
											for (var s = 0; s < this.serverData.view.length; s++) {
												this.wins.push({
													'x': Math.floor(this.serverData.view[s].x*this.displayRatio),
													'y': Math.floor(this.serverData.view[s].y*this.displayRatio),
													"width": Math.floor(this.serverData.view[s].width*this.displayRatio),
													"height": Math.floor(this.serverData.view[s].height*this.displayRatio),
													'color': this.serverData.view[s].color
												});
											}
											this.drawWindows();
										}
										
									}
								},
						response => {

						},
						() => {
							// console.log("The observable is now completed.");
						  }
						);		
			},
			response => {

			},
			() => {

			  }
			);

  }

  mouseDown(e) {
  	console.log(this.wins);
		const rect = this.htmlEl.getBoundingClientRect();
		var mx = Math.floor(e.clientX-rect.left);
		var my = Math.floor(e.clientY-rect.top);
		this.startX = mx;
		this.startY = my;     	

		if (this.addNew) {
			this.wins.push({'x':mx,'y':my,"width":1,"height":1, 'color': '27,185,202'});
			this.drawInProgress = true;
		}else{
			this.selectShape(e.layerX,e.layerY);
		}
  }

  selectShape(mx,my){
		this.selected = false;
		const rect = this.htmlEl.getBoundingClientRect();
		for (var i = 0; i < this.wins.length; i++) {
			var inDrawing = this.inDrawing(mx,my,[this.wins[i].x, this.wins[i].y, this.wins[i].width+this.wins[i].x, this.wins[i].height+this.wins[i].y]);
			if(inDrawing){
				this.selected = true;
				this.selectedWin = i;
				this.winEditForm.patchValue({
					pixelX: Math.floor(this.wins[i].x/this.displayRatio),
					pixelY: Math.floor(this.wins[i].y/this.displayRatio),
					pixelWidth: Math.floor(this.wins[i].width/this.displayRatio),
					pixelHeight: Math.floor(this.wins[i].height/this.displayRatio),
					color: this.wins[i].color,
				});
				var colorEl = this.colorSelection.nativeElement as HTMLCanvasElement;
				colorEl.style.background = "rgba(" + this.winEditForm.value.color + ", 1)";
				this.drawWindows();
			}
		}	
  }

	inDrawing(x, y, rect){
	  if (x >= rect['0'] && x <= rect['2'] && y >= rect['1'] && y <= rect['3']) {
		return true;
	  }else{
		return false;
	  }
	}

  mouseUp(e){
		if (this.addNew) {
			this.addNew = false;
			this.drawInProgress = false;
		}

		if (this.selected) {
			this.selected = false;
		}

  }

  onMouseMove(e){
		const rect = this.htmlEl.getBoundingClientRect();
		if (this.addNew && this.drawInProgress) {
			var newWin = this.wins[this.wins.length-1];
			var mx = e.clientX-(newWin.x+rect.left);
			var my = e.clientY-(newWin.y+rect.top); 	
			newWin.width = Math.floor(mx);
			newWin.height = Math.floor(my);
			this.drawWindows();
		}

		var mxx = e.clientX - rect.left;
		var myy = e.clientY - rect.top;
		var dx = mxx - this.startX;
		var dy = myy - this.startY;
		if (this.selected) {

		if(((this.wins[this.selectedWin].x/this.displayRatio) + Math.floor(dx)) > -1){
			if(((this.wins[this.selectedWin].x/this.displayRatio) + Math.floor(dx)) < (this.screenResolution.x - (this.wins[this.selectedWin].width/this.displayRatio))){
				this.wins[this.selectedWin].x += Math.floor(dx);
			}else{
				this.wins[this.selectedWin].x = (this.screenResolution.x - (this.wins[this.selectedWin].width/this.displayRatio)) * this.displayRatio;
			}
		}else{
			this.wins[this.selectedWin].x = 0;
		}

		if(((this.wins[this.selectedWin].y/this.displayRatio) + Math.floor(dy)) > -1){			
			if(((this.wins[this.selectedWin].y/this.displayRatio) + Math.floor(dy)) < (this.screenResolution.y - (this.wins[this.selectedWin].height /this.displayRatio))){
				this.wins[this.selectedWin].y += Math.floor(dy);
			}else{
				this.wins[this.selectedWin].y = (this.screenResolution.y - (this.wins[this.selectedWin].height /this.displayRatio)) * this.displayRatio;
			}

		}else{
			this.wins[this.selectedWin].y = 0;
		}

		this.winEditForm.patchValue({
			pixelX: Math.floor(this.wins[this.selectedWin].x/this.displayRatio),
			pixelY: Math.floor(this.wins[this.selectedWin].y/this.displayRatio),
			pixelWidth: Math.floor(this.wins[this.selectedWin].width/this.displayRatio),
			pixelHeight: Math.floor(this.wins[this.selectedWin].height/this.displayRatio),
		});

		this.drawWindows();
		this.startX = Math.floor(mxx);
		this.startY = Math.floor(myy);  		
		}
  }	

	getTemplates(screenId){
		this.screenId = screenId;
		this.data.templates(screenId).subscribe(
				(val) => {
					this.templates = val;
					console.log(this.templates);
		},
		response => {

		},
		() => {
			// console.log("The observable is now completed.");
			}
		);		
	}

	addTemplate(){
		this.new_btn = true;
		this.update_btn = false;		
		var tempManage = this.tempManage.nativeElement.style.display;
		if(tempManage == "none"){
			this.draw_controles = false;
			this.tempManage.nativeElement.style.display= "block";
			this.addTemp.nativeElement.style.display= "none";
		}else{
			this.draw_controles = true;
			this.tempManage.nativeElement.style.display= "none";
			this.addTemp.nativeElement.style.display= "block";			
		}
	}
	screenSelection(value){
		var items = value.split("X");
		this.selcetedScreenId = items['0'];
		this.screenResolution.x = items['1'];
		this.screenResolution.y = items['2'];

		this.displayRatio = 650/this.screenResolution.x;

		this.htmlEl.width = 650;
		this.htmlEl.height = this.displayRatio*this.screenResolution.y;
		this.drawWindows();
	}

	drawWindows(){
		this.context.clearRect(0, 0, this.screenResolution.x, this.screenResolution.y);
		var context = this.context;
		var i = 0;
		var selectedWin= this.selectedWin;
		this.wins.forEach(function(win){
			if (i == selectedWin) {
				var rgbaColor = "rgba(" + win.color + ", 0.8)";
			}else{
				var rgbaColor = "rgba(" + win.color + ", 1)";
			}

			context.beginPath();
			context.rect(win.x,win.y,win.width,win.height);
			context.closePath();
			context.stroke();
			context.fillStyle = rgbaColor;	
			context.fill();	
			i++;
		});
	}

	addNewBtn(){
		this.addNew = true;		
	}
	removeWindow(item){
		this.wins.splice(item, 1);
		this.drawWindows();
	}
	colorSelect(e){
		var colorObj = this.hexToRgb(e.target.value);
		this.winEditForm.value.color = colorObj.r + "," + colorObj.g + "," + colorObj.b;
		this.setColor();
	}

	hexToRgb(hex) {
	  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	  return result ? {
		r: parseInt(result[1], 16),
		g: parseInt(result[2], 16),
		b: parseInt(result[3], 16)
	  } : null;
	}	
	zIndex(direction){
		if(direction == "up"){
			if (this.selectedWin < (this.wins.length -1)) {
				moveItemInArray(this.wins, this.selectedWin + 1, this.selectedWin);
				this.selectedWindow(this.selectedWin + 1);
			}
		}
		if(direction == "down"){
			if(this.selectedWin > 0){
				moveItemInArray(this.wins, this.selectedWin - 1, this.selectedWin);
				this.selectedWindow(this.selectedWin - 1);
			}
		}		
	   this.drawWindows();
	}
	manageChanges(step){

	}
	selectedWindow(item){
		this.selectedWin = item;
		this.winEditForm.patchValue({
			pixelX: Math.floor(this.wins[item].x/this.displayRatio),
			pixelY: Math.floor(this.wins[item].y/this.displayRatio),
			pixelWidth: Math.floor(this.wins[item].width/this.displayRatio),
			pixelHeight: Math.floor(this.wins[item].height/this.displayRatio),
			color: this.wins[item].color,
		});
		var colorEl = this.colorSelection.nativeElement as HTMLCanvasElement;
		colorEl.style.background = "rgba(" + this.winEditForm.value.color + ", 1)";
		this.drawWindows();
	}
	syncChanges(field){
		var items = this.winEditForm.value;
		switch(field) {
		  case "pixelX":
			this.wins[this.selectedWin].x = this.displayRatio*items.pixelX;
			break;
		  case "pixelY":
			this.wins[this.selectedWin].y = this.displayRatio*items.pixelY;
			break;
		  case "pixelWidth":
			this.wins[this.selectedWin].width = this.displayRatio*items.pixelWidth;
			break;
		  case "pixelHeight":
			this.wins[this.selectedWin].height = this.displayRatio*items.pixelHeight;
			break;
		}
		this.drawWindows();
	}

	saveTemplate(){
		this.selected = false;		
		this.selectedWin = {};
		this.drawWindows();
		var formItems = this.winEditForm.value;
		var thumbImg = this.htmlEl.toDataURL("image/png");

		for (var i = 0; i < this.wins.length; i++) {
			this.wins[i].x = Math.floor(this.wins[i].x/this.displayRatio);
			this.wins[i].y = Math.floor(this.wins[i].y/this.displayRatio);
			this.wins[i].width = Math.floor(this.wins[i].width/this.displayRatio);
			this.wins[i].height = Math.floor(this.wins[i].height/this.displayRatio);
		}

		var items = {'screenId':this.selcetedScreenId, 'wins':this.wins, 'name':formItems.name, 'thumb':thumbImg};
		this.data.addTemplate(items).subscribe(
			(val) => {
				window.location.reload();
		},
		response => {

		},
		() => {
			// console.log("The observable is now completed.");
		  }
		);		
	}
	updateTemplate(){
		this.selected = false;		
		this.selectedWin = {};
		this.drawWindows();
		var formItems = this.winEditForm.value;
		var thumbImg = this.htmlEl.toDataURL("image/png");

		for (var i = 0; i < this.wins.length; i++) {
			this.wins[i].x = Math.floor(this.wins[i].x/this.displayRatio);
			this.wins[i].y = Math.floor(this.wins[i].y/this.displayRatio);
			this.wins[i].width = Math.floor(this.wins[i].width/this.displayRatio);
			this.wins[i].height = Math.floor(this.wins[i].height/this.displayRatio);
		}

		var items = {'screenId':this.selcetedScreenId, 'templateId': this.templateId, 'wins':this.wins, 'name':formItems.name, 'thumb':thumbImg};
		this.data.updateTemplate(items).subscribe(
			(val) => {
				window.location.reload();
		},
		response => {

		},
		() => {
			// console.log("The observable is now completed.");
		  }
		);		
	}
	editTemplate(id, name){
		const dialogRef = this.dialog.open(EditTemplateDialog, {
		  width: '350px',
		  data: {id: id, name: name}
		});

		dialogRef.afterClosed().subscribe(result => {
		  this.getTemplates(this.screenId)
		});	
	}
	deleteTemplate(id, name){
		const dialogRef = this.dialog.open(DeleteTemplateDialog, {
		  width: '350px',
		  data: {id: id, name: name}
		});

		dialogRef.afterClosed().subscribe(result => {
		  this.getTemplates(this.screenId)
		});
	}	

}


@Component({
  selector: 'delete-dialog',
  templateUrl: 'delete-dialog.html',
})

export class DeleteTemplateDialog {
	@ViewChild('deleteSpinerEl') deleteSpinerEl:ElementRef;
  constructor(
	private dt: PresentationService,
	public dialogRef: MatDialogRef<DeleteTemplateDialog>,
	@Inject(MAT_DIALOG_DATA) public data: DialogData) {}
	lang = lanTranslation;
  onNoClick(): void {
	this.dialogRef.close();
  }

  yesDelete(id){
		var spiner = this.deleteSpinerEl.nativeElement as HTMLCanvasElement;
		//this.appsViewSec = true;
		spiner.style.display = "block";
		this.dt.deleteTemplate(id).subscribe(
			(val) => {
				spiner.style.display = "none";	
				this.dialogRef.close();
				},
		response => {

		},
		() => {
			// console.log("The observable is now completed.");
		  }
		);				
  }

}

@Component({
  selector: 'edit-dialog',
  templateUrl: 'edit-dialog.html',
})

export class EditTemplateDialog {

	EditTemplate = new FormGroup({
		name: new FormControl(''),
	});	
	@ViewChild('editSpinerEl') editSpinerEl:ElementRef;
  constructor(
	private dt: PresentationService,
	public dialogRef: MatDialogRef<EditTemplateDialog>,
	@Inject(MAT_DIALOG_DATA) public data: DialogData) {}
  lang = lanTranslation;
  ngOnInit() {
	this.EditTemplate.patchValue({
	  name: this.data.name
	});  	
  }

  onNoClick(): void {
		this.dialogRef.close();
  }

  saveChanges(id){
		var spiner = this.editSpinerEl.nativeElement as HTMLCanvasElement;
		spiner.style.display = "block";
		this.dt.editTemplate({"id": id, "name": this.EditTemplate.value.name}).subscribe(
			(val) => {
			spiner.style.display = "none";
			this.dialogRef.close();
		},
		response => {

		},
		() => {
			// console.log("The observable is now completed.");
		  }
		) 		
  }

}